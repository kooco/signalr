﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalR.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(string user, string message)
        {
            //var account = Context.User.Claims.FirstOrDefault(r => r.Type == "Account").Value;
            await Clients.All.SendAsync("ReceiveMessage", user, $"Hi {user}，server received your message:{message}");
            //await Clients.All.SendAsync("ReceiveMessage", user, message);
        }
        public async Task SendMessageV2(string user, string message)
        {
            //var account = Context.User.Claims.FirstOrDefault(r => r.Type == "Account").Value;
            await Clients.All.SendAsync("ReceiveMessageV2", user, $"Your connectionId:{Context.ConnectionId}, Message:{message}");
            //await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public override async Task OnConnectedAsync()
        {
            //var account = Context.User.Claims.FirstOrDefault(r => r.Type == "Account").Value;
            //_baseDataProvider.UpdateSignalRConnectionData(account, Context.ConnectionId);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            //_baseDataProvider.RemoveSignalRConnectionData(Context.ConnectionId);
            await base.OnDisconnectedAsync(exception);
        }
    }
}
